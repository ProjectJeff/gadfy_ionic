import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { StorageProvider } from '../storage/storage';
import jsencrypt from 'jsencrypt';
import { MoipCreditCard } from 'moip-sdk-js';

@Injectable()
export class MoipProvider {
  //token moip em base64
  private moip = btoa('WX0KNGAXAKDVCNRTW8E0KX6QLU7QMA0F:SZTQGEDHPJ42I9Z1UWPJW7S7D8JOCDFN8GEZ1V9F');
  public user = null;
  private cpf = null;
  hash: string;

  constructor(public http: HttpClient, public storageP: StorageProvider) {
    //DATA OF USER
    storageP.get('user').then(res => {
    this.user = JSON.parse(res);
  });

}


loadCreditCard(){
  //CREATE HASH CASE CREDIT CARD MOIP DON'T EXIST
  this.storageP.get('credit_card').then(res => {
  this.cpf = JSON.parse(res).cpf;

  const pubKey = `-----BEGIN PUBLIC KEY-----
  MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAs6t8/ZjN1p3ZF74b4vrh
  DqFUrPOqkCeR7LOXMXFk8IP4kcTkURSU6bW0ZjD+2lkiy05ZX683e4EXNdBZtmsB
  bAA0kuVEj4sNcujwzdflhF4W6I3k276Kw6sPoZjrmpZonljtMpAB/cdl8XA0CHqH
  FeBTE01jT3nB+SREA/88D3aMB8UowTKwpQmC3M36erI9ANH2BsGu2JVQrSDIrBH5
  YlQUBnkqM8YJdBQZL/6lW+KlE+n5M3OSZ+QjFA4UUOXEtAJ3ed6kv9HM9/pcgHMc
  mopQiGprKUKTtjmHRXOxJnuZCZvrYDk0Zmbl9FMDlaT/TW+ARQHX6xMLRTP/Gkts
  MwIDAQAB
  -----END PUBLIC KEY-----`;

  MoipCreditCard
  .setEncrypter(jsencrypt, 'ionic')
  .setPubKey(pubKey)
  .setCreditCard({
    number: JSON.parse(res).number,
    cvc: JSON.parse(res).cvc,
    expirationMonth: JSON.parse(res).expirationMonth,
    expirationYear: JSON.parse(res).expirationYear
  })
  .hash()
  .then(hash => { this.hash = hash });
});

}

order(plan) {
  let headers = new HttpHeaders()
  .set("Authorization", "Basic "+this.moip);
  return this.http.post('https://sandbox.moip.com.br/v2/orders',
  {
    "ownId": plan['id'],
    "items": [
      {
        "product": "Plano "+plan['plano'],
        "quantity": 1,
        "detail": plan['cupons']+" tickets",
        "price": plan['valor'].replace(".", "")
      }
    ],
    "customer": {
      "ownId": this.user.id,
      "fullname": this.user.nome,
      "email": this.user.email
    }
  },
  {headers})
}

payment(order) {
  let headers = new HttpHeaders()
  .set("Authorization", "Basic "+this.moip);

  return this.http.post('https://sandbox.moip.com.br/v2/orders/'+order.id+'/payments',
  {
    "installmentCount":1,
    "fundingInstrument":{
      "method":"CREDIT_CARD",
      "creditCard":{
        "hash": this.hash,
        "holder":{
          "fullname":this.user.nome,
          "birthdate":"1993-01-26",
          "taxDocument":{
            "type":"CPF",
            "number":this.cpf
          },
          "phone":{
            "countryCode":"55",
            "areaCode":"51",
            "number":"982890613"
          }
        }
      }
    }
  },
  {headers})
}

}
