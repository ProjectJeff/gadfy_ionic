import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

@Injectable()
export class StorageProvider {

  constructor(public http: HttpClient, public storage: Storage) {
    console.log('Hello StorageProvider Provider');
  }

  store(name, value) {
    return this.storage.set(name, JSON.stringify(value));
  }

  get(name) {
    return this.storage.get(name);
  }

  remove(name) {
    return this.storage.remove(name);
  }

  removeAll() {
    return this.storage.clear();
  }

}
