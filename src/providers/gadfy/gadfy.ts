import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

  @Injectable()
  export class GadfyProvider {
    uri = 'http://ec2-18-231-179-88.sa-east-1.compute.amazonaws.com/api/';
    //uri = 'http://localhost:8000/api/';
  	constructor(public http: HttpClient) {}

  	load(position, page, category) {
  		return this.http.get(this.uri+'address?lat='+position.lat+'&lng='+position.lng+'&page='+page+'&category_id='+category);
  	}

    getPlaceDetail(place_id) {
      return this.http.get(this.uri+'place/'+place_id);
    }

    createUser(user) {
      return this.http.post(this.uri+'user/add', user);
    }

    getUser(user){
      return this.http.get(this.uri+'user?email='+user.email+'&senha='+user.senha);
    }

    getPlans(){
      return this.http.get(this.uri+'plans');
    }

    createCreditCard(card){
      return this.http.post(this.uri+'credit_card/add', card);
    }

    removeCard(user_gadfy_id) {
      return this.http.delete(this.uri+'credit_card/remove?user_gadfy_id='+user_gadfy_id);
    }

    getCard(user_gadfy_id) {
      return this.http.get(this.uri+'credit_card?user_gadfy_id='+user_gadfy_id);
    }

    buyPlan(plan) {
      return this.http.post(this.uri+'tickets/buy', plan);
    }

    getTickets(user_gadfy_id) {
      return this.http.get(this.uri+'tickets/'+user_gadfy_id);
    }

    burnCode(data) {
      return this.http.post(this.uri+'tickets/generate_code', data);
    }

    getCategories() {
      return this.http.get(this.uri+'categories');
    }

  }
