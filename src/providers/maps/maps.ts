import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

  @Injectable()
  export class MapsProvider {
  	data: any;

  	constructor(public http: HttpClient) {}

  	load(address) {
            return this.http.get('https://maps.google.com/maps/api/geocode/json?address=' + address + '&sensor=false&key=AIzaSyDLaapEiapp4inlQUmzalv-0CNUMPYpas4');
        }
  }
