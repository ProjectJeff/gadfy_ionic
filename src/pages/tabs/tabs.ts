import { Component } from '@angular/core';

import { FeedPage } from '../feed/feed';
import { HomePage } from '../home/home';
import { MenuPage } from '../menu/menu';
import { FavoritePage } from '../favorite/favorite';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = FeedPage;
  tab3Root = FavoritePage;
  tab4Root = MenuPage;

  constructor() {

  }
}
