import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ModalController } from 'ionic-angular';
import { GadfyProvider } from '../../providers/gadfy/gadfy';
import { AlertController } from 'ionic-angular';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { Geolocation } from "@ionic-native/geolocation";
import { PlaceDetailPage } from '../place-detail/place-detail';
import { CouponsPage } from '../coupons/coupons';
import { CategoryPage } from '../category/category';

import { StorageProvider } from '../../providers/storage/storage';

declare var google;

 @IonicPage()
 @Component({
   selector: 'page-feed',
   templateUrl: 'feed.html',
 })
 export class FeedPage {

   public locais = [];
   public positionStart : any;
   public page = 1;
   public useCategory = null;

   constructor(public navCtrl: NavController,
     public navParams: NavParams,
     private gadfyApi: GadfyProvider,
     private alertCtrl: AlertController,
     public loadingCtrl: LoadingController,
     private geolocation: Geolocation,
     public storageP: StorageProvider,
     public modal: ModalController) {}

   ionViewDidLoad() {
     this.getPosition();
   }

   doRefresh(refresher) {
     this.getLocais(this.positionStart);
     setTimeout(() => {
       refresher.complete();
     }, 1500);
   }

   getPosition() {
       this.geolocation.getCurrentPosition()
           .then((resp) => {
               //captura local atual
               this.positionStart = {'lat': resp.coords.latitude, 'lng': resp.coords.longitude};
               this.getLocais(this.positionStart);
           }).catch((error) => {
           console.log('Erro ao recuperar sua posição', error);
           this.normalAlert('Não foi possível encontrar sua localização atual, ative o GPS para continuar.');
       });
   }

   getLocais(position) {
     return new Promise((resolve, reject) => {
     this.gadfyApi.load(position, this.page, this.useCategory)
     .subscribe(data => {
       if(this.page > 1){
         for (let i in data[0]){
           this.locais[0].push(data[0][i]);
         }
       }
       if(this.page == 1){
         this.locais = [];
         this.locais.push(data[0]);
       }
        resolve(true);
       console.log(this.locais);
     }, err => {
       reject(false);
       console.log(err);
     });
    })
   }


   convertKm(distance) {
       return distance.toFixed(2) + ' KM';
   }

   convertUrlPerfil(url) {
     return url.replace("public","");
   }

   normalAlert(message) {
     let alert = this.alertCtrl.create({
       title: 'Atenção',
       subTitle: message,
       buttons: ['fechar']
     });
     alert.present();
   }

   loadingDefault() {
     return this.loadingCtrl.create({
       content: 'Carregando seu guia...',
     });
   }

   placeDetail(place){
     this.navCtrl.push(PlaceDetailPage, {
       'place': place
     });
   }

   doInfinite(infiniteScroll) {
    setTimeout(() => {
        this.page = this.page+1;
        this.getLocais(this.positionStart).then((result => {
            infiniteScroll.complete();
        })
      )
    }, 5000);
  }


  couponsModal() {
    let cpoupon = this.modal.create(CouponsPage);
    cpoupon.present();

    cpoupon.onDidDismiss(data => {
      console.log(data);
    });
  }

  categoryModal() {
    let category = this.modal.create(CategoryPage);
    category.present();

    category.onDidDismiss(data => {
      if(data){
        this.useCategory = data.category_id;
        this.locais = [];
        this.getLocais(this.positionStart);
      }
      console.log(data);
    });
  }

  exit() {
    this.storageP.removeAll();
  }


 }
