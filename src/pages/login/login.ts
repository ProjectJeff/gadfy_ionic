import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';
import { AlertController } from 'ionic-angular';
import { GadfyProvider } from '../../providers/gadfy/gadfy';
import { StorageProvider } from '../../providers/storage/storage';

import { CreateUserPage } from '../create-user/create-user';
import { TabsPage } from '../tabs/tabs';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  userFb = null;
  usergadfy = {'email': null, 'senha': null};

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public facebook: Facebook,
    private alertCtrl: AlertController,
    private gadfy: GadfyProvider,
    public storageP: StorageProvider) {}

    ionViewDidLoad() {
      console.log('ionViewDidLoad LoginPage');
    }

    //método para chamar api do facebook e salvar no banco o usuario
    loginFacebook() {
    this.facebook.login(['public_profile', 'email'])
    .then((response: FacebookLoginResponse) => {
      if(response.status == "connected") {
        // Get user ID and Token
        var fb_id = response.authResponse.userID;
        var fb_token = response.authResponse.accessToken;

        this.facebook.api("/me?fields=id,name,email,picture.width(720).height(720).as(picture_large)&access_token=" + fb_token, []).then(profile => {
          this.userFb = {
            'name': profile['name'],
            'email': profile['email'],
            'fone': '51999999999',
            'senha': profile['id'],
            'picture': profile['picture_large']['data']['url'],
            'login_fb': true,
            'login_insta': false};

            this.gadfy.getUser(this.userFb).subscribe(res => {
              if(res['user'].length > 0){
                //CREATE SESSION USER
                this.storageP.store('user', res['user'][0]);

                return this.navCtrl.push(TabsPage);
              }else{
                return this.createUserFb();
              }
            });
          });
        }
      });
    }

    createUserFb() {
      this.gadfy.createUser(this.userFb).subscribe(res => {
        if(res['message'] == 'success'){
          //CREATE SESSION USER
          this.storageP.store('user', res['user']);
          return this.navCtrl.push(TabsPage);
        }
        return this.alert('Ocorreu um erro ao entrar, tente novamente.');
      });
    }

    logingadfy() {
      this.gadfy.getUser(this.usergadfy).subscribe(res => {
        if(res['user'].length > 0){
          //CREATE SESSION USER
          this.storageP.store('user', res['user'][0]);

          return this.navCtrl.push(TabsPage);
        }
        return this.alert('E-mail ou senha incorreta');
      })
    }

    createUser(){
      return this.navCtrl.push(CreateUserPage);
    }

    alert(message) {
      let alert = this.alertCtrl.create({
        title: 'Atenção',
        subTitle: message,
        buttons: ['fechar']
      });
      alert.present();
    }


  }
