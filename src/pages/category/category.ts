import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { GadfyProvider } from '../../providers/gadfy/gadfy';

@IonicPage()
@Component({
  selector: 'page-category',
  templateUrl: 'category.html',
})
export class CategoryPage {

  public category = {
    1: 'beer.png',
    2: 'restaurante.png',
    3: 'shopping.png',
    4: 'mecanica.png',
    5: 'pizza.png',
    6: 'hamburguer.png',
    7: 'barbearia.png',
    8: 'madeireira.png',
    9: 'hotel.png',
    10: 'estacionamento.png',
    11: 'shopping.png',
    12: 'academia.png'
  };

  categories = null;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public gadfy: GadfyProvider,
              public viewCtrl: ViewController) {
  }

  ionViewDidLoad() {
    this.getCategories();
  }

  getCategories() {
    this.gadfy.getCategories().subscribe(res => {
      this.categories = res['categories'];
      console.log(this.categories);
    })
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  useCategory(category_id) {
    this.viewCtrl.dismiss({category_id: category_id});
  }

}
