import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { GadfyProvider } from '../../providers/gadfy/gadfy'
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { LoginPage } from '../login/login';
import { AlertController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-create-user',
  templateUrl: 'create-user.html',
})
export class CreateUserPage {
  private user : FormGroup;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public gadfy: GadfyProvider,
              public formBuilder: FormBuilder,
              public alertCtrl: AlertController) {

                this.user = formBuilder.group({
                    name: ['', Validators.required],
                    email: ['', [Validators.required, Validators.email,  Validators.pattern('^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$')]],
                    fone : ['', [Validators.required]],
                    senha: ['', Validators.compose([Validators.minLength(6), Validators.maxLength(20),
                    Validators.required])],
                    retry_password: ['', Validators.compose([Validators.minLength(6), Validators.maxLength(20),
                    Validators.required])],
                    picture: [''],
                    login_fb: [false],
                    login_insta: [false]
                  });
    }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CreateUserPage');
  }

  createUser() {
    console.log(this.user.value);
    if(this.user.value.senha != this.user.value.retry_password){
      return this.normalAlert('Senhas não conferem');
    }
    this.gadfy.createUser(this.user.value).subscribe(res => {
        if(res['message'] == 'success'){
          this.normalAlert('Cadastro realizado.');
          return this.navCtrl.pop();
        }
        return this.normalAlert('Ocorreu um erro ao cadastrar, tente novamente mais tarde.');
    }, err => {
      return this.normalAlert('Ocorreu um erro ao cadastrar, tente novamente mais tarde.');
    });
  }


  redirectFeed() {
    return this.navCtrl.push(TabsPage);
  }

  normalAlert(message) {
    let alert = this.alertCtrl.create({
      title: 'Atenção',
      subTitle: message,
      buttons: ['fechar']
    });
    alert.present();
  }

}
