import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { GadfyProvider } from '../../providers/gadfy/gadfy';
import { StorageProvider } from '../../providers/storage/storage';

@IonicPage()
@Component({
  selector: 'page-place-detail',
  templateUrl: 'place-detail.html',
})
export class PlaceDetailPage {
	@ViewChild('mySlider') mySlider: any;
	place_start: any;
  place: any;
  user_gadfy_id = null;

  constructor(public navCtrl: NavController,
  			      public navParams: NavParams,
              public gadfy: GadfyProvider,
              public storageP: StorageProvider,
              public alertCtrl: AlertController) {
              this.place_start = this.navParams.get('place');
  }

  ionViewDidLoad() {
    this.gadfy.getPlaceDetail(this.place_start.id)
    .subscribe(res => {
      console.log(res['place'][0]);
      this.place = res['place'][0];
    });
    this.storageP.get('user').then(res => {
      this.user_gadfy_id = JSON.parse(res).id;
    });
  }

  goback() {
   return this.navCtrl.pop();
  }

  convertKm(distance) {
     if (distance >= 1000) {
       return (distance/1000).toFixed(2) + ' km';
     } else {
       return distance.toFixed(2) + ' metros';
     }
   }

   slideNext(){
       this.mySlider.slideNext();
     }

     slidePrev(){
       this.mySlider.slidePrev();
     }

     convertUrlImage(url) {
       return url.replace("public","");
     }

     getDescount(descount_id) {
       let data = {desconto_id : descount_id, user_gadfy_id: this.user_gadfy_id};

       this.gadfy.burnCode(data).subscribe(res => {
         if(res['message'] == 'success'){
           return this.sampleAlert('Obrigado', 'Este é o número do seu ticket: '+res['codigo']);
         }
         return this.sampleAlert('Atenção', 'Ocorreu um erro ao resgatar este ticket');
       });
     }

     confirmDescount(descount_id) {
     let alert = this.alertCtrl.create({
       title: 'Atenção',
       subTitle: 'Deseja resgatar este ticket?',
       buttons: [
         {
           text: 'Não',
           role: 'Não',
           handler: () => {
             console.log('Cancel clicked');
           }
         },
         {
           text: 'Sim',
           handler: () => {
             this.getDescount(descount_id);
           }
         }
       ]
     });
     alert.present();
   }

   sampleAlert(title, message) {
     let alert = this.alertCtrl.create({
       title: title,
       subTitle: message,
       buttons: ['Fechar']
     });
     alert.present();
   }

}
