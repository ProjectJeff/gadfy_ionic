import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Nav } from 'ionic-angular';
import { ProfilePage } from '../profile/profile';
import { NotificationPage } from '../notification/notification';
import { CreditCardPage } from '../credit-card/credit-card';
import { CouponsPage } from '../coupons/coupons';
import { HelpPage } from '../help/help';

import { StorageProvider } from '../../providers/storage/storage';

@IonicPage()
@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html',
})
export class MenuPage {

  @ViewChild(Nav) public nav: Nav;

  name = null;
  public pages = [
    {title: 'Perfil', component: ProfilePage, icon: 'person'},
    {title: 'Cartão de crédito', component: CreditCardPage, icon: 'card'},
    {title: 'Notificações', component: NotificationPage, icon: 'notifications-outline'},
    {title: 'Comprar tickets', component: CouponsPage, icon: 'cash'},
    {title: 'Como usar', component: HelpPage, icon: 'help'},
  ];

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public storageP: StorageProvider) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad MenuPage');
    this.storageP.get('user').then(res => {
      this.name = JSON.parse(res).nome;
    });
  }

  redirect(component){
    this.navCtrl.push(component);
  }

}
