import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { GadfyProvider } from '../../providers/gadfy/gadfy';
import { StorageProvider } from '../../providers/storage/storage';

@IonicPage()
@Component({
  selector: 'page-credit-card',
  templateUrl: 'credit-card.html',
})
export class CreditCardPage {

  card = {
    'cpf' : null,
    'number': null,
    'expirationDate': null,
    'cvc': null
  };
  user = null;
  card_exist = null;
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public gadfy: GadfyProvider,
              public storageP: StorageProvider,
              public alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log(this.card_exist);
    this.storageP.get('user').then(res => {
        this.user = JSON.parse(res);
    });
    this.storageP.get('credit_card').then(res => {
        if(res != null){
          this.card_exist = JSON.parse(res);
        }
    });
  }

  createCreditCard(){
    let card = this.parseCreditCard(this.card);
    this.storageP.store('credit_card', card);
    return this.card_exist = card;
  }

  parseCreditCard(card){
    return {
      'cpf' : card.cpf.replace('.', '').replace('.', '').replace('-', ''),
      'number': card.number.replace('.', '').replace('.', '').replace('.', ''),
      'expirationMonth': card.expirationDate.split('/')[0],
      'expirationYear': card.expirationDate.split('/')[1],
      'cvc': card.cvc,
      'user_gadfy_id': this.user.id
    }
  }


  removeCard() {
    this.gadfy.removeCard(this.card_exist.user_gadfy_id).subscribe(res =>{
      if(res['message'] == 'success'){
        this.storageP.remove('credit_card');
        this.card_exist = null;
        return this.normalAlert('Cartão removido');
      }
        return this.normalAlert('Ocorreu um erro ao remover o cartão');
      },
        err => {
          return this.normalAlert('Ocorreu um erro ao remover o cartão');
      });
  }

  normalAlert(message) {
    let alert = this.alertCtrl.create({
      title: 'Atenção',
      subTitle: message,
      buttons: ['fechar']
    });
    alert.present();
  }

}
