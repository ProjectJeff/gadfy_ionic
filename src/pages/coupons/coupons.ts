import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { LoadingController, AlertController } from 'ionic-angular'

import { GadfyProvider } from '../../providers/gadfy/gadfy';
import { MoipProvider } from '../../providers/moip/moip';
import { StorageProvider } from '../../providers/storage/storage';

import { CreditCardPage } from '../credit-card/credit-card';

@IonicPage()
@Component({
  selector: 'page-coupons',
  templateUrl: 'coupons.html',
})
export class CouponsPage {

  plans = null;
  loading: any;
  credit_card: any;
  user_gadfy_id = null;
  moip_pedido = null;
  tickets = 0;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public viewCtrl: ViewController,
              public gadfy: GadfyProvider,
              public moip: MoipProvider,
              public storageP: StorageProvider,
              public loadingCtrl: LoadingController,
              private alertCtrl: AlertController) {
      this.storageP.get('user').then(res => {
        this.user_gadfy_id = JSON.parse(res).id;
      });
  }

  ionViewDidLoad() {
    this.getTickets();
    this.getPlans();
    this.storageP.get('credit_card').then(res => {
      this.credit_card = JSON.parse(res);
    });
    //Carregar dados do cartão antecipadamente
    this.moip.loadCreditCard();
  }

  ionViewDidEnter(){
    this.getTickets();
    this.storageP.get('credit_card').then(res => {
      this.credit_card = JSON.parse(res);
    });
    //Carregar dados do cartão antecipadamente
    this.moip.loadCreditCard();
  }

  getPlans() {
    this.gadfy.getPlans().subscribe(res => {
      return this.plans = res['plans'];
    })
  }

  purshaseCoupon(plan) {
      if(!this.credit_card){
        return this.redirectCreateCard();
      }
      //CREATE ORDER
      this.presentLoadingCustom();
      this.moip.order(plan).subscribe(val => {
        this.moip_pedido = val['id'];
        //CREATE PAYMENT OF ORDER
        this.moip.payment(val).subscribe(val => {
        this.loading.dismissAll();
          this.buyPlan(this.user_gadfy_id, plan.id, plan.cupons, this.moip_pedido, val['id']);
        }, err => {
          this.loading.dismissAll();
          this.getTickets();
          this.sampleAlert('Atenção!', err['error'].errors[0].description+'. Verifique seus dados de cartão de crédito');
        });
      }, err => {
        this.loading.dismissAll();
        this.sampleAlert('Atenção!', 'Ocorreu um erro ao gerar seu pedido, tente novamente mais tarde.');
      });
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  confirmPlan(plan) {
  let alert = this.alertCtrl.create({
    title: 'Atenção',
    subTitle: 'Contratar o plano '+plan['plano']+' '+plan['cupons']+' tickets',
    buttons: [
      {
        text: 'Não',
        role: 'Não',
        handler: () => {
          console.log('Cancel clicked');
        }
      },
      {
        text: 'Sim',
        handler: () => {
          this.purshaseCoupon(plan);
        }
      }
    ]
  });
  alert.present();
}

sampleAlert(title, message) {
  let alert = this.alertCtrl.create({
    title: title,
    subTitle: message,
    buttons: ['Fechar']
  });
  alert.present();
}


redirectCreateCard() {
  let alert = this.alertCtrl.create({
    title: 'Atenção!',
    subTitle: 'Para realizar esta compra você precisa vincular um cartão de crédito.',
    buttons: [
      {
        text: 'Vincular depois',
        role: 'Vincular depois',
        handler: () => {
          console.log('Cancel clicked');
        }
      },
      {
        text: 'Vincular',
        handler: () => {
          this.navCtrl.push(CreditCardPage);
        }
      }
    ]
  });
  alert.present();
}

  presentLoadingCustom() {
    this.loading = this.loadingCtrl.create({
      //spinner: 'hide',
      spinner: 'hide',
      content: `<img class="loading" width="100%" src="assets/imgs/loading.gif" />`
    });

    this.loading.present();
  }

  buyPlan(user_gadfy_id, plan, qtd_tickets, moip_pedido, moip_pagamento) {
    this.gadfy.buyPlan({ user_gadfy_id, plan, qtd_tickets, moip_pedido, moip_pagamento }).subscribe(res => {
      return this.sampleAlert('Obrigado!', 'Dentro de alguns minutos você receberá seus tickets.');
    });
  }

  getTickets() {
    this.storageP.get('user').then(res => {
      this.gadfy.getTickets(JSON.parse(res).id).subscribe(res =>{
        this.tickets = res['tickets'];
      });
    });
  }

}
