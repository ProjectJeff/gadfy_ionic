import { Component } from '@angular/core';
import { NavController, ModalController, NavParams, ViewController } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { MapsProvider } from '../../providers/maps/maps';
import { GadfyProvider } from '../../providers/gadfy/gadfy';
import { AlertController } from 'ionic-angular';
import { Renderer } from '@angular/core';
import { PlaceDetailPage } from '../place-detail/place-detail';
import { IonPullUpFooterState } from 'ionic-pullup';

declare var google;

@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})
export class HomePage {

    footerState: IonPullUpFooterState;
    directionsService = new google.maps.DirectionsService();
    directionsDisplay = new google.maps.DirectionsRenderer();
    map: any;
    startPosition: any;
    locais = [];
    page = 1;
    //cria array de marcadores de locais
    markers = [];
    local = null;
    useCategory = null;

    constructor(public navCtrl: NavController,
                private geolocation: Geolocation,
                private gmaps: MapsProvider,
                private gadfy: GadfyProvider,
                private alertCtrl: AlertController) {
      this.footerState = IonPullUpFooterState.Collapsed;
    }

    /*IONIC PULL UP http://arielfaur.github.io/ionic-pullup/*/
    footerExpanded() {
      console.log('Footer expanded!');
    }
    footerCollapsed() {
      console.log('Footer collapsed!');
    }
    toggleFooter() {
      this.footerState = this.footerState == IonPullUpFooterState.Collapsed ? IonPullUpFooterState.Expanded : IonPullUpFooterState.Collapsed;
    }
    /*END IONIC PULL UP*/

    ionViewDidLoad() {
        //inicializa o mapa
        this.initializeMap().then(result => {
          //RETORNA OS LUGARES PASSADOS POR API
          this.getAddress().then(result => {
            this.getLocais(this.locais);
          });
        });
    }

    initializeMap() {
        //PERMIÇÃO PARA PEGAR LOCAL ATUAL
        return new Promise((resolve, reject) => {
        this.geolocation.getCurrentPosition()
            .then((resp) => {
                //captura local atual
                const position = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude);
                this.startPosition = {'lat': resp.coords.latitude, 'lng': resp.coords.longitude};
                //options root
                const mapOptions = {
                    zoom: 14,
                    center: position,
                    mapTypeId: 'roadmap',
                    disableDefaultUI: true,
                    styles:
                        [
                            {
                                "elementType": "geometry",
                                "stylers": [
                                    {
                                        "color": "#f5f5f5"
                                    }
                                ]
                            },
                            {
                                "elementType": "labels.icon",
                                "stylers": [
                                    {
                                        "visibility": "off"
                                    }
                                ]
                            },
                            {
                                "elementType": "labels.text.fill",
                                "stylers": [
                                    {
                                        "color": "#616161"
                                    }
                                ]
                            },
                            {
                                "elementType": "labels.text.stroke",
                                "stylers": [
                                    {
                                        "color": "#f5f5f5"
                                    }
                                ]
                            },
                            {
                                "featureType": "administrative.land_parcel",
                                "elementType": "labels.text.fill",
                                "stylers": [
                                    {
                                        "color": "#bdbdbd"
                                    }
                                ]
                            },
                            {
                                "featureType": "poi",
                                "elementType": "geometry",
                                "stylers": [
                                    {
                                        "color": "#eeeeee"
                                    }
                                ]
                            },
                            {
                                "featureType": "poi",
                                "elementType": "labels.text.fill",
                                "stylers": [
                                    {
                                        "color": "#757575"
                                    }
                                ]
                            },
                            {
                                "featureType": "poi.park",
                                "elementType": "geometry",
                                "stylers": [
                                    {
                                        "color": "#e5e5e5"
                                    }
                                ]
                            },
                            {
                                "featureType": "poi.park",
                                "elementType": "labels.text.fill",
                                "stylers": [
                                    {
                                        "color": "#9e9e9e"
                                    }
                                ]
                            },
                            {
                                "featureType": "road",
                                "elementType": "geometry",
                                "stylers": [
                                    {
                                        "color": "#ffffff"
                                    }
                                ]
                            },
                            {
                                "featureType": "road.arterial",
                                "elementType": "labels.text.fill",
                                "stylers": [
                                    {
                                        "color": "#757575"
                                    }
                                ]
                            },
                            {
                                "featureType": "road.highway",
                                "elementType": "geometry",
                                "stylers": [
                                    {
                                        "color": "#dadada"
                                    }
                                ]
                            },
                            {
                                "featureType": "road.highway",
                                "elementType": "labels.text.fill",
                                "stylers": [
                                    {
                                        "color": "#616161"
                                    }
                                ]
                            },
                            {
                                "featureType": "road.local",
                                "elementType": "labels.text.fill",
                                "stylers": [
                                    {
                                        "color": "#9e9e9e"
                                    }
                                ]
                            },
                            {
                                "featureType": "transit.line",
                                "elementType": "geometry",
                                "stylers": [
                                    {
                                        "color": "#e5e5e5"
                                    }
                                ]
                            },
                            {
                                "featureType": "transit.station",
                                "elementType": "geometry",
                                "stylers": [
                                    {
                                        "color": "#eeeeee"
                                    }
                                ]
                            },
                            {
                                "featureType": "water",
                                "elementType": "geometry",
                                "stylers": [
                                    {
                                        "color": "#c9c9c9"
                                    }
                                ]
                            },
                            {
                                "featureType": "water",
                                "elementType": "labels.text.fill",
                                "stylers": [
                                    {
                                        "color": "#9e9e9e"
                                    }
                                ]
                            }
                        ]
                }

                //setando mapa no html
                this.map = new google.maps.Map(document.getElementById('map'), mapOptions);
                this.directionsDisplay.setMap(this.map);
                this.addMyPosition(position);
                resolve(true);
            }).catch((error) => {
              this.normalAlert('Não foi possível encontrar sua localização atual, ative o GPS para continuar.');
              reject(false);
        });
      });
    }

    addMyPosition(position){
        //incluindo o marcador local
        this.markers.push(new google.maps.Marker({
            position: position,
            icon: 'assets/icon/myposition.png',
            map: this.map,
            animation: google.maps.Animation.DROP
        }));
    }


    addPullUp(marker, local) {
      marker.addListener('click', () => {
        this.local = local;
      });
    }


    addMarkers(local, local_detail) {
      let mark = null;
      this.markers.push(mark = new google.maps.Marker({
        position: local,
        icon: 'assets/icon/location.png',
        map: this.map,
        animation: google.maps.Animation.DROP
      }));
      this.addPullUp(mark, local_detail);
    }

    getLocais(address) {
      for(let i = 0; i < address[0].length; i++) {
        this.gmaps.load(address[0][i].endereco).subscribe(data => {
          //criando marcação de lugares no mapa
          this.addMarkers(data['results'][0].geometry.location, address[0][i]);
        });
      }
    }

    getAddress() {
        return new Promise((resolve, reject) => {
        this.gadfy.load(this.startPosition, this.page, this.useCategory)
        .subscribe(data => {
          if(this.page > 1){
            for (let i in data[0]){
              this.locais[0].push(data[0][i]);
            }
          }
          if(this.page == 1){
            this.locais.push(data[0]);
          }
           resolve(true);
        }, err => {
          reject(false);
          console.log(err);
        });
       })
    }

    normalAlert(message) {
        let alert = this.alertCtrl.create({
            title: 'Atenção',
            subTitle: message,
            buttons: ['fechar']
        });
        alert.present();
    }

    placeDetail(){
      this.navCtrl.push(PlaceDetailPage, {
        'place': this.local
      });
    }

    convertKm(distance) {
        return distance.toFixed(2) + ' KM';
    }

}
