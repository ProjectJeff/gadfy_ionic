import { NgModule, ErrorHandler, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { ProfilePage } from '../pages/profile/profile';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { FeedPage } from '../pages/feed/feed';
import { PlaceDetailPage } from '../pages/place-detail/place-detail';
import { MenuPage } from '../pages/menu/menu';
import { NotificationPage } from '../pages/notification/notification';
import { FavoritePage } from '../pages/favorite/favorite';
import { CreditCardPage } from '../pages/credit-card/credit-card';
import { CouponsPage } from '../pages/coupons/coupons';
import { HelpPage } from '../pages/help/help';
import { LoginPage } from '../pages/login/login';
import { CreateUserPage } from '../pages/create-user/create-user';
import { CategoryPage } from '../pages/category/category';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { Geolocation } from '@ionic-native/geolocation';
import { MapsProvider } from '../providers/maps/maps';
import { HttpClientModule } from '@angular/common/http';
import { GadfyProvider } from '../providers/gadfy/gadfy';
import { MoipProvider } from '../providers/moip/moip';
import { Facebook } from '@ionic-native/facebook';
import { IonicStorageModule } from '@ionic/storage';
import { StorageProvider } from '../providers/storage/storage';
import { BrMaskerModule } from 'brmasker-ionic-3';
import { IonPullupModule } from 'ionic-pullup';

@NgModule({
  declarations: [
    MyApp,
    ProfilePage,
    HomePage,
    TabsPage,
    FeedPage,
    PlaceDetailPage,
    MenuPage,
    NotificationPage,
    FavoritePage,
    CreditCardPage,
    CouponsPage,
    HelpPage,
    LoginPage,
    CreateUserPage,
    CategoryPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp,{
      autocomplete: 'off'
    }),
    HttpClientModule,
    IonicStorageModule.forRoot(),
    BrMaskerModule,
    IonPullupModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    ProfilePage,
    HomePage,
    TabsPage,
    FeedPage,
    PlaceDetailPage,
    MenuPage,
    NotificationPage,
    FavoritePage,
    CreditCardPage,
    CouponsPage,
    HelpPage,
    LoginPage,
    CreateUserPage,
    CategoryPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
     Geolocation,
    MapsProvider,
    GadfyProvider,
    MoipProvider,
    Facebook,
    StorageProvider
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class AppModule {}
