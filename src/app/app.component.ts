import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { TabsPage } from '../pages/tabs/tabs';
import { LoginPage } from '../pages/login/login';
import { StorageProvider } from '../providers/storage/storage';



@Component({
  selector: 'myapp',
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, storageP: StorageProvider) {
    //VERIFICA SE USUARIO JÁ ESTEVE LOGADO
    storageP.get('user').then((val) => {
      if(JSON.parse(val) != null){
        this.rootPage = TabsPage;
      }else{
        this.rootPage = LoginPage;
      }
    });

    platform.ready().then(() => {
      statusBar.styleDefault();
      statusBar.backgroundColorByHexString('#FFFFFF');
      splashScreen.hide();
    });
  }

}
